'use strict';

// requirements
const express = require('express');
const payment = require('./payment');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => {
    res.status(200).end();
});
app.head('/status', (req, res) => {
    res.status(200).end();
});

var isSafe32BitInt = value => {
    try {
        var valueNum = Number(value);
        if (Number.isNaN(valueNum)) {
            return false;
        }
        return Number(value) < (Math.pow(2, 32) / 2) && Number(value) >= -(Math.pow(2, 32) / 2);
    } catch (ex) {
        //console.log(ex);
        return false;
    }
}


var validateAmount = (amount) => {
    if (Number(amount) > 0 && isSafe32BitInt(amount)) {
        return Number(amount);
    }

    // can throw instead
    return false;
};

var validateAction = (action) => {
    // type string
    if (typeof (action) !== 'string') {
        return false;
    }

    const normalisedAction = action.normalize("NFC");

    if (
        normalisedAction === 'transfer'
        && normalisedAction.toLowerCase().match(/^[a-zA-Z]*$/)
    ) {
        return normalisedAction;
    }

    return false;
}

app.get('/', (req, res) => {
    // We only allow transfer
    // TODO: add str buf to read only n bytes from req
    const validatedAction = validateAction(req.query.action);
    const validatedAmount = validateAmount(req.query.amount);

    if (!!validatedAction && !!validatedAmount) {
        res.send(payment(validatedAction, validatedAmount));
        return;
    }

    res.status(400).send('You can only transfer an amount');
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function () {
    process.exit();
});

module.exports = app;
